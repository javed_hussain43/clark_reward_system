class AddAncestryToReferrals < ActiveRecord::Migration[5.2]
  def change
    add_column :referrals, :ancestry, :string
    add_column :referrals, :ancestry_depth, :integer, default: 0
    add_index :referrals, :ancestry
  end
end
