class CreateReferrals < ActiveRecord::Migration[5.2]
  def change
    create_table :referrals do |t|
      t.string :user_id
      t.string :inviter_id
      t.boolean :is_joined, default: false

      t.timestamps
    end
    add_index :referrals, :user_id
    add_index :referrals, :inviter_id
  end
end
