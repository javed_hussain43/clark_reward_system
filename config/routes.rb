Rails.application.routes.draw do
	namespace :api do
  	namespace :v1 do
  		resources :rewards, only: [] do
  			post 'calculate', on: :collection
  		end
  	end
  end
end
