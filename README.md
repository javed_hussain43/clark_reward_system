# Reward App

A company is planning a way to reward customers for inviting their friends. They're planning a reward system that will give a customer points for each confirmed invitation they played a part into. The definition of a confirmed invitation is one where an invited person accepts their contract. Inviters also should be rewarded when someone they have invited invites more people.

The inviter gets (1/2)^k points for each confirmed invitation, where k is the level of the invitation: level 0 (people directly invited) yields 1 point, level 1 (people invited by someone invited by the original customer) gives 1/2 points, level 2 invitations (people invited by someone on level 1) awards 1/4 points and so on. Only the first invitation counts: multiple invites sent to the same person don't produce any further points, even if they come from different inviters and only the first invitation counts.


# The Solution
In the soluton, we will store the referral record in the database. And each row will stores:

1. user_id (ID of the user)

2. inviter_id (ID of user who have invited you)

3. is_joined (Flag to check whether the user has accpted the recommendation or not)

4. ancestry (Basically we are storing the ancestors of the TreeNode)

5. ancestry_depth (This is store the depth of the TreeNode)


Here basically we are storing the records in TreeNodes. For example,

                  A
                /   \
               B     C
             /         \ 
            D            G
          /   \
        E      F

A -> { Level_0 = [B,C], Level_1 = [D,G], Level_2 = [E,F] }

B -> { Level_0 = [D], Level_1 = [E,F]}

C -> { Level_0 = [G] }

D -> { Level_0 = [E,F]}

E, F -> { [] }

Calculation

A = (1+1), (1/2+1/2), (1/4+1/4) = 3.5

B = (1), (1/2+1/2) = 1.5




# How to Build and Run

Please make sure you have installed ruby verison '2.6.4'


1. First of all clone it and cd /clark_reward_system

2. Bundle install and rake db:create db:migrate

3. Start the rails server [rails s] and check in the browser http://localhost:3000 

4. Any rest client or terminal make a POST API CALL

  * URL - http://localhost:3000/api/v1/rewards/calculate

  * ContentType - Application/Json

  * Parameters - file: FILE_URL


5. We can also make a curl call from terminal.

curl -X POST \
  http://localhost:3000/api/v1/rewards/calculate \
  -H 'Accept: */*' \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F file=@/Users/Javed/workspace/clark_reward_system/test/fixtures/files/reward_system.csv




