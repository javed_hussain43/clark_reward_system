class Api::V1::RewardsController < ApplicationController
	FILE_FORMAT = 'text/csv'
	skip_before_action :verify_authenticity_token

	def calculate
		if params_file && params_file.content_type == FILE_FORMAT
			result, errors = Services::CalculateReward.new(params_file).call
			render json: { 
				result: result, 
				errors: errors 
			}
		else
			render json: { errors: 
				{ 
					message: translate('file_not_found')
				},
			},
			status: :bad_request
		end
	end

	private

	def params_file
		params[:file]
	end
end