class Referral < ApplicationRecord
	scope :without_empty_descendants, -> { where.not(ancestry: nil) }

	has_ancestry cache_depth: true
end
