module UseCases
	class ReferralRecords
		RECOMMENDS = 'recommends'.freeze
		BASE_SCORE = "1/2".to_r

		attr_reader :row

		def initialize(row)
			@row = row
		end
		
		def process_row
			if row[:action_type].downcase == RECOMMENDS
				inviter = Referral.find_or_create_by(user_id: row[:inviter], is_joined: true)
				invitee = Referral.create(user_id: row[:invitee], inviter_id: inviter.user_id, parent: inviter) unless Referral.exists?(user_id: row[:invitee])
			else
				referral = Referral.find_by(user_id: row[:inviter])
				referral.update_attribute(:is_joined, true)
			end
		end

		
		def self.calculate_referral_score
			result = {}
			Referral.without_empty_descendants.each do |referral|
	    	referral_score = 0
	    	ancestors_count = referral.ancestors.count
	    	descendants = referral.descendants
	    	descendants.each do |descendant|
	    		iterator_times = (descendant.ancestry_depth - ancestors_count - 1)
	    		referral_score += BASE_SCORE ** iterator_times
	    	end
	    	result[referral.user_id] = referral_score.to_f
	    end
	    result
		end

	end
end