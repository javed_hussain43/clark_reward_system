# app/services/calculate_reward.rb
require 'csv'
module Services
	class CalculateReward
		attr_reader :file, :errors


		def initialize(file)
			@file = file
			@errors = []
		end

		def call
			CSV.foreach(file.path, headers: true).with_index do |row, index|
				row_hash = row.to_hash.deep_symbolize_keys
				error = RewardCsvRowValidator.new(row_hash, index).validate
				if error.blank?
					referral_records.new(row_hash).process_row
				else
					errors.push(error)
				end
	    end
	    result = referral_records.calculate_referral_score 	
			return result, errors
		end

		private

		def referral_records
			UseCases::ReferralRecords
		end

	end
end
