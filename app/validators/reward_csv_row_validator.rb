#<RewardCsvRowValidator:0x00007fcd9be61458
# @errors=
#  {"row_1"=>
#    {:date=>{:message=>"Incorrect Date Format YYYY-MM-DD HH:MM AM"},
#     :inviter=>{:message=>"User name should be a string"},
#     :invitee=>{:message=>"User name should be a string"},
#     :action_type=>{:message=>"ActionType can only be accepts, recommends"}}},
# @index_key="row_1",
# @row={"date"=>"x", "inviter"=>"1", "action_type"=>"y", "invitee"=>"1"}>
# This is a custom validator to validate each row fo the CSV before processing.

class RewardCsvRowValidator

	ACTION_TYPE = %w(accepts recommends)
	NAME_REGEX = /\A[a-zA-Z]+\z/

	attr_reader :row, :index_key

	attr_accessor :error
	
	def initialize(row, index)
    @row = row
    @error = {}
    @index_key = "row_#{index+1}"
  end

  def validate
  	assign_empty_row_error
  	validate_date
  	validate_inviter_name
  	validate_invitee_name
  	validate_action_type
  	error["#{index_key}"].blank? ? {} : error
  end


  private

  def assign_empty_row_error
  	error["#{index_key}"] = {} 
  end

  def validate_date
  	row_date = row[:date] && DateTime.parse(row[:date]) rescue false
  	unless row_date
  		error["#{index_key}"][:date] = {
  			message: I18n.t('incorrect_date_format')
  		}
  	end
  end

  def validate_inviter_name
  	row_inviter = row[:inviter]
  	unless row_inviter && NAME_REGEX =~ row_inviter
  		error["#{index_key}"][:inviter] = {
  			message: I18n.t('incorrect_user_name')
  		}
  	end
  end

  def validate_invitee_name
  	row_invitee = row[:invitee].nil? ? row[:action_type] == ACTION_TYPE.first ? true : false :  NAME_REGEX =~ row[:invitee]
  	unless row_invitee
  		error["#{index_key}"][:invitee] = {
  			message: I18n.t('incorrect_user_name')
  		}
  	end
  end

  def validate_action_type
  	row_action = row[:action_type]
  	unless row_action && ACTION_TYPE.include?(row_action)
  		error["#{index_key}"][:action_type] = {
  			message: I18n.t('action_type_warning')
  		}
  	end
  end
end