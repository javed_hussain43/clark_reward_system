require 'rails_helper'

describe RewardCsvRowValidator do
	let!(:index) {1}
	subject { RewardCsvRowValidator.new(row, index ).validate }

	context 'valid data' do
		let!(:row) { {date: '2018-06-12 9:41', inviter: 'A', action_type: 'recommends', invitee: 'B'} }

		it '#validate input data and should return empty {}' do
			expect(subject).to eq({})
		end
	end

	context '#invalid data' do
		let!(:row) { {date: '3', inviter: '1', action_type: 'x', invitee: '2'} }

		it '#action_type and should return error' do
			expect(subject["row_#{index+1}"].keys).to include(:action_type)
			expect(subject["row_#{index+1}"][:action_type][:message]).to eq(I18n.t('action_type_warning'))
		end

		it '#date and should return error' do
			expect(subject["row_#{index+1}"].keys).to include(:date)
			expect(subject["row_#{index+1}"][:date][:message]).to eq(I18n.t('incorrect_date_format'))
		end

		it '#inviter and should return error' do
			expect(subject["row_#{index+1}"].keys).to include(:inviter)
			expect(subject["row_#{index+1}"][:inviter][:message]).to eq(I18n.t('incorrect_user_name'))
		end

		it '#invitee and should return error' do
			expect(subject["row_#{index+1}"].keys).to include(:invitee)
			expect(subject["row_#{index+1}"][:invitee][:message]).to eq(I18n.t('incorrect_user_name'))
		end
	end
end