require 'rails_helper'

RSpec.describe Referral, type: :model do

	context 'Scope without_empty_descendants' do
		let!(:referral) do
			5.times{|index| Referral.create(user_id: "A_#{index}", inviter_id: "B_#{index}") }
		end

		it 'scope should return blank array' do
			expect(Referral.without_empty_descendants).to eq([])
		end

		it 'scope should return one object' do
			parent_referral = Referral.first
			last_referral = Referral.last 
			last_referral.update_attribute(:ancestry, parent_referral)
			expect(Referral.without_empty_descendants.count).to eq(1)
			expect(Referral.without_empty_descendants.first).to eq(last_referral)
		end
	end

end
