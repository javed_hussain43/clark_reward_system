#spec/services/calculate_reward.rb
require 'rails_helper'

describe Services::CalculateReward do
	describe 'calculate_reward' do
		context 'valid parameters' do
			let!(:file) { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/reward_system.csv'), 'text/csv') }
			let!(:expected_response) do
				[
	        {
	          "B": 1.5,
	          "C": 1.0,
	          "D": 0.0
	        },
	        []
	      ]
			end
			subject { Services::CalculateReward.new(file).call }


			it 'should return valid response' do
				expect(subject).to eq(expected_response.as_json)
			end
		end
		context 'invalid parameters' do
      let!(:file) { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/reward_input_invalid.csv'), 'text/csv')}
      let!(:expected_response) do
        [
          {"row_1"=>{"invitee"=>{"message"=>"User name should be a string"}}},
          {"row_2"=>{"inviter"=>{"message"=>"User name should be a string"}}},
          {"row_3"=>{"date"=>{"message"=>"Incorrect Date Format YYYY-MM-DD HH:MM AM"}, "invitee"=>{"message"=>"User name should be a string"}}},
          {"row_4"=>{"action_type"=>{"message"=>"ActionType can only be accepts, recommends"}}}
        ]
      end

			subject { Services::CalculateReward.new(file).call }


			it 'should return error response' do
				expect(subject.second.as_json).to eq(expected_response.as_json)
			end
		end
	end
end

