# spec/controllers/api/v1/rewards_controller_spec.rb
require 'rails_helper'

RSpec.describe "API V1 Rewards", type: :request do
  describe "POST /api/v1/rewards" do
    
    before do
      post '/api/v1/rewards/calculate', params: { "file" => file }
    end
    
    context "with valid parameters" do
      let!(:file) { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/reward_system.csv'), 'text/csv')}
      let!(:expected_response) do
        {
          "result": {
            "B": 1.5,
            "C": 1.0,
            "D": 0.0
          },
          "errors": []
        }
      end

      it "returns http success and contains expected attributes" do
        expect(response).to have_http_status(:success)
        expect(response.content_type).to eq("application/json")
      end

      it 'should contains expected attributes' do
        json_response = JSON.parse(response.body)
        expect(json_response.keys).to match_array(["result", "errors"])
        expect(json_response).to eq(expected_response.as_json)
      end
    end

    context 'without parameters' do
      let!(:file) {}
      it "returns http success and contains expected attributes" do
        expect(response).to have_http_status(:bad_request)
        expect(response.content_type).to eq("application/json")
      end

      it 'should contains expected attributes' do
        json_response = JSON.parse(response.body)
        expect(json_response.keys).to match_array(["errors"])
        expect(json_response.dig("errors", "message")).to eq(I18n.t('file_not_found'))
      end
    end

    context 'with invalid parameters' do
      let!(:file) { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/reward_input_invalid.csv'), 'text/csv')}
      let!(:expected_response) do
        {
          "result"=>{},
          "errors"=>
          [
            {"row_1"=>{"invitee"=>{"message"=>"User name should be a string"}}},
            {"row_2"=>{"inviter"=>{"message"=>"User name should be a string"}}},
            {"row_3"=>{"date"=>{"message"=>"Incorrect Date Format YYYY-MM-DD HH:MM AM"}, "invitee"=>{"message"=>"User name should be a string"}}},
            {"row_4"=>{"action_type"=>{"message"=>"ActionType can only be accepts, recommends"}}}
         ]
       }
      end
      it "returns http success and contains expected attributes" do
        expect(response).to have_http_status(:success)
        expect(response.content_type).to eq("application/json")
      end

      it 'should contains expected attributes' do
        json_response = JSON.parse(response.body)
        expect(json_response.keys).to match_array(["result", "errors"])
        expect(json_response).to eq(expected_response.as_json)
      end
    end
  end
end